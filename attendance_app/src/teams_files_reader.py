from src.helpers import *
from src.report_section_helper import *
from src.summary_builder import normalize_summary
from src.participant_builder import resolve_participant_join_last_time
from src.meeting_builder import normalize_meeting
from tools.logger_wrapper import setup_logger


log_normalize_dict = setup_logger("Normalize Dictionary")


def get_summary_as_dict(raw_data):
    return dict([row for _, row in raw_data]) 


def get_data_as_dict(data):
    if data == []: return data

    _, header = data[0] # TODO must guarante index 0 exists
    return [dict(zip(header,row)) for _, row in data]


def normalize_raw_data(data):
    is_old_file_version = [MEETING_SUMMARY] in data

    # extract data sections
    summary =  extract_section_rows(data, MEETING_SUMMARY, FULL_NAME) if is_old_file_version else extract_section_rows(data, '1. Summary', PARTICIPANTS)
    participants  = [] #if is_old_file_version else extract_section_rows(data, PARTICIPANTS, ACTIVITIES)
    in_meetings = extract_section_rows(data, FULL_NAME, include_start=True) if is_old_file_version else extract_section_rows(data, ACTIVITIES)

    # convert list that contains "summary" lines into dict 
    summary = get_summary_as_dict(summary)
    # participants = get_data_as_dict(participants)
    in_meetings = get_data_as_dict(in_meetings)
    # Eliminate the first sublist because there are the headers
    in_meetings.pop(0)

    summary =  normalize_summary(summary)
    participants = resolve_participant_join_last_time(in_meetings)
    in_meetings = [normalize_meeting(in_meeting) for in_meeting in in_meetings ]

    return ({"1. Summary": summary, "2. Participants": participants, "3. In-Meeting activities": in_meetings}, is_old_file_version)


def load_data(file_paths)-> tuple[list, list, list, int, int]:
    log_normalize_dict.info("Process Starts")
    normalized_data_list = list()
    success_list = list()
    error_acumulator = list()
    ok_files_acumulator = 0
    old_version_counter = 0
    new_version_counter = 0

    for file_path in file_paths:
        try:
            raw_data = get_file_data(file_path)
            normalized_data, is_old_version = normalize_raw_data(raw_data)
            if is_old_version:
                old_version_counter +=1
            else: 
                new_version_counter += 1
            number_of_participants_in_meet = normalized_data.get('1. Summary').get('Attended participants')
            normalized_data_list.append(normalized_data)
            success_list.append(f'{number_of_participants_in_meet}, {file_path}')
            ok_files_acumulator += 1
        except Exception as e: 
            log_normalize_dict.info(f"Up this point {ok_files_acumulator} were processed OK")
            log_normalize_dict.info(f"--- ERROR --- || File: {file_path} || Exception: {e}")
            log_normalize_dict.info(f"--- Continue with the process ---")
            error_acumulator.append(file_path)
            continue

    return (normalized_data_list, error_acumulator, success_list, old_version_counter, new_version_counter)
