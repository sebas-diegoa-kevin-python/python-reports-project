from src.duration_helper import *
from src.helpers import FORMAT_OLD, FULL_NAME, FORMAT_NEW


def normalize_summary(raw_data: dict):
    is_old = "Meeting Id" in raw_data
    title = raw_data.get("Meeting title") or raw_data.get("Meeting Title")
    meeting_id = raw_data.get('Meeting Id') or title
    number_of_participants = raw_data.get("Attended participants") or raw_data.get("Total Number of Participants")
    start_time = raw_data.get("Start time") or raw_data.get("Meeting Start Time")
    end_time = raw_data.get("End time") or raw_data.get("Meeting End Time")
    #TO DO
    in_meeting_duration = get_time(get_meeting_duration(start_time, end_time, FORMAT_OLD)) if is_old else get_time(get_meeting_duration(start_time, end_time, FORMAT_NEW))
    #in_meeting_duration = raw_data.get('Meeting duration') or get_time(get_meeting_duration(start_time, end_time, FORMAT_OLD))
    normalized_dict = {
        'Meeting title': title,
        'Id':meeting_id,
        'Attended participants': number_of_participants,
        'Start Time': start_time,
        'End Time': end_time,
        'Meeting duration': in_meeting_duration,
    }
    return normalized_dict


def build_summary_object(raw_data: dict):
    pass