from src.duration_helper import get_meeting_duration, get_time
from src.helpers import FORMAT_OLD, FORMAT_NEW, FULL_NAME

def normalize_meeting(raw_data: dict):
    is_old = FULL_NAME in raw_data
    join_time =  raw_data.get("Join time") or raw_data.get('Join Time')
    leave_time = raw_data.get("Leave time") or raw_data.get('Leave Time')
    normalized_dict = {
        "Name": raw_data.get("Name") or raw_data.get(FULL_NAME),
        "Join time": join_time,
        "Leave time": leave_time,
        "Duration": get_time(get_meeting_duration(join_time, leave_time,FORMAT_OLD)) if is_old else get_time(get_meeting_duration(join_time, leave_time,FORMAT_NEW)),
        "Email":  raw_data.get("Email"),
        "Role": raw_data.get("Role")
    }
    return normalized_dict