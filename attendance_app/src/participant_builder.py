from datetime import timedelta
from src.duration_helper import get_meeting_duration, get_time
from src.helpers import FORMAT_OLD, FORMAT_NEW, FULL_NAME


def sort_data(raw, col):
    return sorted(raw, key = lambda i: i[col])


def participant_in_meeting(participant, order_data):
    if participant.get('Email') == '':
        return [participant]
    return [comparator for comparator in order_data if participant.get('Email') == comparator.get('Email')]


def validate_in_data(participant, unique_data):
    if participant.get('Email') == '': return False
    for data in unique_data: 
        if participant.get('Email') == data.get('Email'): return True

def get_unique_data(order_data):
    unique_data = []
    if unique_data == []:
        unique_data.append(order_data[0])
    [unique_data.append(participant) for participant in order_data if not validate_in_data(participant, unique_data)]
    return unique_data


def create_participant(participant_duplicates):
    is_old = FULL_NAME in participant_duplicates[0]
    duration = timedelta()
    order_duplicates = sort_data(participant_duplicates, 'Join Time') if is_old else sort_data(participant_duplicates, 'Join time')
    start_time = order_duplicates[0]['Join Time'] if is_old else order_duplicates[0]['Join time']
    end_time = order_duplicates[-1]['Leave Time'] if is_old else order_duplicates[-1]['Leave time']
    for item in order_duplicates:
        duration += get_meeting_duration(item.get('Join Time'), item.get('Leave Time'), FORMAT_OLD) if is_old else get_meeting_duration(item.get('Join time'), item.get('Leave time'), FORMAT_NEW)

    return {
        'Name': order_duplicates[0].get('Full Name') if is_old else order_duplicates[0].get('Name'),
        'First join': start_time,
        'Last leave': end_time,
        'In-meeting duration': get_time(duration),
        'Email': order_duplicates[0].get('Email'),
        'Participant ID (UPN)': order_duplicates[0].get('Email'),
        'Role': order_duplicates[0].get('Role')
    }


def resolve_participant_join_last_time(raw: dict):
    is_old = FULL_NAME in raw[0]
    order_data = sort_data(raw, 'Join Time') if is_old else sort_data(raw, 'Join time')
    unique_data = get_unique_data(order_data)
    unique_participants = []
    for participant in unique_data:
        participant_duplicates = participant_in_meeting(participant, order_data)
        unique_participants.append(create_participant(participant_duplicates))
    return unique_participants


def build_participant_object(raw: dict):
    pass