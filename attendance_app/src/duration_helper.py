from datetime import datetime


def format_time(time, date_format):
    return datetime.strptime(time, date_format)


def get_meeting_duration(start, end, date_format):
    return format_time(end, date_format) - format_time(start, date_format)


def duration_format(hours, minutes, seconds):
    return f'{hours}h {minutes}m {seconds}s'
    
    
def get_time(meeting_duration):
    seconds = meeting_duration.seconds
    hours, rest = divmod(seconds, 3600)
    minutes, seconds = divmod(rest, 60)
    return duration_format(hours, minutes, seconds)




# def convert_duration_str_to_dict(duration_str):
#     return dict()


# def convert_dict_to_object():
#     return Dictionary()