import csv
import os
import sys

file = __file__
dirname = os.path.dirname(file)
dirname = dirname[::-1].split('\\', 1)[-1][::-1]
sys.path.append(dirname)

SUMMARY_DATE_FORMAT = "%Y-%m-%d %H:%M:%S"
FOLDER_DATE_NAME = '%Y%m%d%H%M%S'

def create_file_sumary_report(start_time_excecution, end_time_excecution, number_of_files_in_folder, old_version_counter, new_version_counter, number_of_error_files, number_of_success_files, folder_destination_loggs):
    with open(folder_destination_loggs+f"/sumary_{start_time_excecution.strftime(FOLDER_DATE_NAME)}.txt", 'w') as summary_file:
        summary_file.write("Summary \n")
        summary_file.write('\n')
        summary_file.write(f'Excecution Start: {start_time_excecution.strftime(SUMMARY_DATE_FORMAT)}\n')
        summary_file.write(f'Excecution End: {end_time_excecution.strftime(SUMMARY_DATE_FORMAT)}\n')
        summary_file.write(f"Number of files in folder: {number_of_files_in_folder}\n")
        summary_file.write(f"Number of 'meetingAttendanceReport Files: {old_version_counter}\n")
        summary_file.write(f"Number of NON 'meetingAttendanceReport Files: {new_version_counter}\n")
        summary_file.write(f"Total number of files with error: {number_of_error_files}\n")
        summary_file.write(f"Total number of files with success: {number_of_success_files}\n")



def create_folder(folder_destination, start_time_excecution):
    route_folder = f"{folder_destination}{start_time_excecution}"
    os.makedirs(route_folder, exist_ok=True)
    return route_folder


def convert_to_csv(summary, participants, activities, name_file, folder_destination):
    folder_and_file = os.path.join(folder_destination, name_file)

    with open(folder_and_file, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter='\t')
        writer.writerow(['1. Summary'])
        for key, value in summary.items():
            writer.writerow([f"{key}: {value}"])
        writer.writerow([])

        writer.writerow(['2. Participants'])
        writer.writerow(participants[0].keys())
        for participant in participants:
            writer.writerow(participant.values())
        writer.writerow([])

        writer.writerow(['3. In Meeting activities'])
        writer.writerow(activities[0].keys())
        for activity in activities:
            writer.writerow(activity.values())
    return csvfile


def process_create_folder_and_csv_file(data_loaded, success_path_list, folder_destination, start_time_excecution):
    current_path = ""
    #folder = create_folder(folder_destination.replace('\\', '/'), start_time_excecution.strftime(FOLDER_DATE_NAME))
    folder = create_folder(folder_destination, start_time_excecution.strftime(FOLDER_DATE_NAME))
    for index, data in enumerate(data_loaded):
        current_path = success_path_list[index]
        file_name = current_path.split('/')[-1]
        convert_to_csv(
            data.get('1. Summary'),
            data.get('2. Participants'),
            data.get('3. In-Meeting activities'),
            file_name,
            folder
        )
    return folder


def generate_success_file(success_path_list, folder_destination_loggs):
    with open(folder_destination_loggs+'/success.txt', 'w', newline='') as succes_file:
        if len(success_path_list) ==0:
            succes_file.write('0')
        else:
            for path in success_path_list:
                succes_file.write(path + '\n')


def generate_error_file(error_acumulator, folder_destination_loggs):
    with open(folder_destination_loggs+'/error.txt', 'w', newline='') as error_file:
        if len(error_acumulator) ==0:
            error_file.write('0')
        else:
            for item in error_acumulator:
                error_file.write(item + '\n')
