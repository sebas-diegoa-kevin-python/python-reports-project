from os import scandir
import csv
from colorama import Fore
from pathlib import Path


MEETING_SUMMARY = 'Meeting Summary'
FULL_NAME = 'Full Name'
PARTICIPANTS = '2. Participants'
ACTIVITIES = '3. In-Meeting activities'

FORMAT_OLD = '%m/%d/%Y, %I:%M:%S %p'
FORMAT_NEW = '%m/%d/%y, %I:%M:%S %p'

WHITE = Fore.WHITE
GREEN = Fore.GREEN
LIGHTRED = Fore.LIGHTRED_EX


parent_path = Path(__file__).parent.parent
path_data = parent_path.joinpath('data').resolve().__str__()


def discover_data_files(folder=path_data, find_pattern='.csv'):
    with scandir(folder) as files:
        return [folder + '/' + file.name for file in files if
                file.is_file() and file.name.endswith(find_pattern)]


def get_file_data(path):
    with open(path, encoding='UTF-16') as csv_file:
        return [line for line in csv.reader(csv_file, delimiter='\t')]


def input_folder_path(is_origin):
    choice = ''
    while choice not in ['Y', 'N']:
        choice = (input(GREEN+"Do you want to specify Origin path?  (Y/N) : "+WHITE)).upper() if is_origin else (input(GREEN+"Do you want to specify Destination path?  (Y/N) : "+WHITE)).upper()
        if choice == 'Y':
            path_folder = input(GREEN+'Specify path: '+WHITE)
            return path_folder
        elif choice == 'N':
            return
        else:
            print(LIGHTRED+'Incorrect Value try again'+WHITE)

