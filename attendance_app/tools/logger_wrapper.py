import logging


def setup_logger(logger_name, logfile="log.log"):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.INFO)

    # create file handler which logs even debug messages
    file_handler = logging.FileHandler(logfile)
    file_handler.setLevel(logging.INFO)

    # create console handler with a higher log level
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)

    # add the handlers to the logger
    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger
