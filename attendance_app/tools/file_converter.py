import sys
import os

file = __file__
dirname = os.path.dirname(file)
dirname = dirname[::-1].split('\\', 1)[-1][::-1]
sys.path.append(dirname)

from src.data_file_converter import process_create_folder_and_csv_file, generate_success_file, generate_error_file, create_file_sumary_report
from src.helpers import discover_data_files
from src.teams_files_reader import load_data
from src.helpers import input_folder_path
from logger_wrapper import setup_logger
from datetime import datetime
from pathlib import Path


parent_path = Path(__file__).parent.parent
path_data = parent_path.joinpath('stage_data').resolve().__str__()


def main():
    log_program_execution = setup_logger("--- Program Execution ---")
    log_program_execution.info("-- START ---")
    
    start_time_excecution = datetime.now()
    
    # Ask User for origin Path
    folder_path_origin = input_folder_path(True)
    # Ask User for destiny Path
    folder_path_destination = input_folder_path(False) or path_data

    # Check destiny Path
    if not folder_path_destination.endswith("/"):
        folder_path_destination += "/"

    # Get the Path of the list of files
    if folder_path_origin:
        file_paths = discover_data_files(folder_path_origin) or []
    else:
        file_paths = discover_data_files() or []

    log_normalized_data = setup_logger("Normalize Data Process")

    # Normalized data
    log_normalized_data.info("--- START ---")
    data_loaded, error_acumulator, success_path_list, old_version_counter, new_version_counter = load_data(file_paths)
    log_normalized_data.info("--- END ---")

    folder_destination_loggs = process_create_folder_and_csv_file(data_loaded, success_path_list, folder_path_destination, start_time_excecution)

    generate_success_file(success_path_list, folder_destination_loggs)
    generate_error_file(error_acumulator, folder_destination_loggs)

    number_of_success_files = len(success_path_list)
    number_of_error_files = len(error_acumulator)
    number_of_files_in_folder = len(file_paths)

    end_time_excecution = datetime.now()

    create_file_sumary_report(start_time_excecution, end_time_excecution, number_of_files_in_folder, old_version_counter, new_version_counter, number_of_error_files, number_of_success_files, folder_destination_loggs)

    log_program_execution.info("-- END ---")

if __name__ == "__main__":
    main()
