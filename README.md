## Instructions

- Keep in mind your main goal is to write anything you need to get the unit tests running successfully, but:
  - You cannot change existing module names because those modules are referenced by other projects already. 
  - You cannot change existing function's names, signatures because those function are referenced by other projects already. 
  - You can add as many class definitions, constructors, methods as you need but for those existing classes you cannot rename them or remove any existing constructor

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/sebas-diegoa-kevin-python/python-reports-project.git
python -m venv ./venv
pip install -r requirements.txt

##Build with 
pytest==7.2.0
colorama==0.4.4
```

## Start the project
python file_converter.py

## Select the option you want to use
Y or N for the Origin Path
Y or N for the Destination Path

if u select Y you need to write the path (Absolute Path)
if u select N the default path will be used

## Description
We have the paths of origin and destination for te create the files into the destination path, the process is the next:
1. We read the files from the origin path
2. We Normalize the data from the files (Summary, Participants, In - Meetings Activities)
3. We create the files according to the New version of the files into the destination path
4. We create the logs.logs into the 'tools' folder
5. We create Success.txt, Error.txt and the Summary.txt into the destination path with the the required information
